// server.js

// To do:
// - Separate Route and View
// - Add Model
// - Use eslint

// SETUP
// App
const express    = require('express');           // call express
const app        = express();                    // define our app using express
const port       = process.env.PORT || 3000;      // set our port

// Libraries
const bodyParser = require('body-parser');

const firebase   = require('firebase');
const config     = require('./config.json');

// Json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// Firebase Configs
firebase.initializeApp(config);

var db = firebase.app().database();
app.set("db", db)

// Some Logging (May be Necessary)
/*
app.use((req, res, next) => {
    console.log('Incoming request'); // Logs into terminal
    next();
});
*/

// ROUTES
app.use('/api',require('./routers/api/api'));
app.use('/',require('./routers/home'));


// START
app.listen(port);
console.log('Server is served on port ' + port);
