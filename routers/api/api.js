// /api/

// Testing Guide:
// 1. run server using `node server.js` on terminal
// 2. run `curl localhost:8080/api -d @testing.json --header "Content-Type: application/json"`

// SETUP
const express = require('express'),
  router = express.Router();
const url = require("url");
const path = require("path");
///const sanitizer = require("sanitize")();

// ROUTES (/api/)
router.use('/user',require('./user'));

// OTHERS
module.exports = router;
