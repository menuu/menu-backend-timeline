// /user/

// Testing Guide:
// 1. run server using `node server.js` on terminal
// 2. run `curl localhost:8080/api -d @testing.json --header "Content-Type: application/json"`

// SETUP
const express = require('express'),
  router = express.Router();
const url = require("url");
const path = require("path");
///const sanitizer = require("sanitize")();

// ROUTES

// ===============================================================
// /user/
var userRoute = router.route('/:key?');
// Create
userRoute.post((req, res) => {
  let reqBody = req.body;
  // ! remember to do sanitizing here later on

  // Creating
  let db = req.app.get('db');

  let newKey = db.ref().child('user').push().key;
  reqBody["key"] = newKey;

  let updates = {};
  updates['/user/'+newKey] = reqBody;

  db.ref().update(updates);

  //console.log(reqBody);

  // Finished
  console.log("POST Request");
  res.json({"Action":"CREATE","Status":"Successful", "key":newKey});
});

// READ
userRoute.get((req,res)=>{
  let key = req.params.key;
  let db = req.app.get('db');

  // Reading
  db.ref('user/'+key).once('value').then((snap) => {
    let db_read = snap.val();

    console.log("READ Request");
    res.json(db_read);
  });
});

// Update
userRoute.put((req,res)=>{
  let reqBody = req.body;
  // ! remember to do sanitizing here later on

  let key = req.params.key;

  // Updating
  let db = req.app.get('db');

  let updates = {};
  updates['/user/'+key] = reqBody;

  db.ref().update(updates);

  // Finished
  console.log("PUT Request");
  res.json({"Action":"UPDATE","Status":"Successful", "key":key});
});

// Delete
userRoute.delete((req,res)=>{
  let key = req.params.key;
  console.log(key)

  // Deleting
  let db = req.app.get('db');

  // Finished
  db.ref('user/'+key).remove();
  console.log("DELETE Request");
  res.json({"Action":"DELETE","Status":"Successful", "key":key});
});

// OTHERS
module.exports = router;
